<h2>Hi there &#128075;, I'm Dhananjay Bipin Pai! <img src="https://bardotbrush.com/wp-content/uploads/2019/04/Untitled_Artwork-2.gif" width="50"></h2>
<img align='right' src="https://blog.commlabindia.com/wp-content/uploads/2019/07/animated-gifs-corporate-training.gif" width="230">
<p><em>Pursuing Bachelor of Engineering(BE) in CSE at <a href="https://vesit.ves.ac.in/">Vivekanand Education Society's Institute Of Technology</a>
</em></p>
 A little more about me...  

```python
#!/usr/bin/python
# -*- coding: utf-8 -*-
class SoftwareEngineer:
    def __init__(self):
        self.name = "Dhananjay Pai"
        self.role = "Software Engineer"
        self.language_spoken = ["en","hi"]
 
    def ask_me_about(self):
        self.answer = ["Anime", "Movies", "Sports", "Tech"]
    def say_hi(self):
        print("Thanks for dropping by, hope you find some of my work interesting.")
me = SoftwareEngineer()
me.say_hi()
```

## 🔧 Technologies & Tools

![](https://img.shields.io/badge/OS-Windows-informational?style=flat&logo=windows&logoColor=white&color=6aa6f8)
![](https://img.shields.io/badge/Editor-VS_Code-informational?style=flat&logo=visual-studio-code&logoColor=white&color=6aa6f8)
![](https://img.shields.io/badge/Code-Python-informational?style=flat&logo=python&logoColor=white&color=6aa6f8)
![](https://img.shields.io/badge/Code-Java-informational?style=flat&logo=java&logoColor=white&color=6aa6f8)
![](https://img.shields.io/badge/Code-C++-informational?style=flat&logo=c++&logoColor=white&color=6aa6f8)
![](https://img.shields.io/badge/Code-JavaScript-informational?style=flat&logo=javascript&logoColor=white&color=6aa6f8)
![](https://img.shields.io/badge/Code-C-informational?style=flat&logo=c&logoColor=white&color=6aa6f8)
![](https://img.shields.io/badge/Tools-SQLite-informational?style=flat&logo=sqlite&logoColor=white&color=6aa6f8)
![](https://img.shields.io/badge/Tools-Flask-informational?style=flat&logo=flask&logoColor=white&color=6aa6f8)
![](https://img.shields.io/badge/Tools-Django-informational?style=flat&logo=django&logoColor=white&color=6aa6f8)

## 🏆 GitHub Trophies

[![trophy](https://github-profile-trophy.vercel.app/?username=dhananjaypai08&theme=nord&column=7)](https://github.com/ryo-ma/github-profile-trophy)

##  GitHub Stats
<a href="https://github.com/dhananjaypai08/dhananjaypai08">
  <img align="center" src="https://github-readme-stats.vercel.app/api/top-langs/?username=dhananjaypai08&hide=c%2B%2B,c,matlab,assembly&title_color=6aa6f8&text_color=8a919a&icon_color=6aa6f8&bg_color=22272e" alt="Dhananjay's GitHub Stats" />
</a>
<a href="https://github.com/dhananjaypai08/dhananjaypai08">
  <img align="center" src="https://github-readme-stats.vercel.app/api?username=dhananjaypai08&show_icons=true&line_height=27&count_private=true&title_color=6aa6f8&text_color=8a919a&icon_color=6aa6f8&bg_color=22272e" alt="Dhananjay's GitHub Stats" />
</a> -->


## 🗂️ Highlight Projects

<a href="https://github.com/dhananjaypai08/WeatherCheck">
  <img align="center" src="https://github-readme-stats.vercel.app/api/pin/?username=dhananjaypai08&repo=WeatherCheck&show_icons=true&line_height=27&title_color=6aa6f8&text_color=8a919a&icon_color=6aa6f8&bg_color=22272e" alt="WeatherCheck" />
</a>

<a href="https://github.com/dhananjaypai08/Login-Page">
  <img align="center" src="https://github-readme-stats.vercel.app/api/pin/?username=dhananjaypai08&repo=Login-Page&show_icons=true&line_height=27&title_color=6aa6f8&text_color=8a919a&icon_color=6aa6f8&bg_color=22272e" alt="Login-Page" />
</a>

<img src="https://media.giphy.com/media/LnQjpWaON8nhr21vNW/giphy.gif" width="60"> <em><b>I love interacting with different people</b> so if you want to say <b>hi!, I'll be happy to connect! :) </b> 😊</em>
<br>
<img src="https://media.tenor.com/images/53676346a29801bdbf00c768004645d8/tenor.gif" width='200'>
